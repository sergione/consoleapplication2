﻿using System;
using System.Collections.Generic;
using Ninject;
using Ninject.Extensions.Conventions;

namespace ConsoleApplication2
{
    /// <summary>
    /// A wrapper around teh Ninject kernel
    /// </summary>
    public class ServiceLocator
    {
        public static readonly ServiceLocator Instance = new ServiceLocator();

        private readonly Lazy<StandardKernel> _kernel = new Lazy<StandardKernel>(InitializeKernel);

        private static StandardKernel InitializeKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind(x =>
            {
                x.FromThisAssembly().SelectAllClasses().BindAllInterfaces();
            });

            
            return kernel;
        }

        private ServiceLocator()
        {
            
        }

        public T Get<T>()
        {
            return _kernel.Value.Get<T>();
        }

        public IEnumerable<T> GetAll<T>()
        {
            return _kernel.Value.GetAll<T>();
        }

        public object Get(Type type)
        {
            return _kernel.Value.Get(type);
        }
    }
}
