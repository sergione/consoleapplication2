using System;
using ConsoleApplication2.Concept.Events;
using ConsoleApplication2.Events;

namespace ConsoleApplication2
{
    public class SayHelloRequestHandler : RequestHandler<SayHelloRequest, EmptyResponse>
    {
        public override void Execute()
        {
            Console.WriteLine("Hello from the Request handler" + Request.Name);

            var helloSaidEvent = new HelloSaid(Request.Name);

            ApplicationEvents.Raise(helloSaidEvent);
        }

        protected override IValidate CreatePermissionValidator()
        {
            return this.HasPermissions("test permission");
        }
    }
}