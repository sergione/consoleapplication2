using FluentValidation;

namespace ConsoleApplication2
{
    public class SayHelloRequest : IRequest<EmptyResponse>
    {
        public string Name { get; set; }

        public IValidate CreateValidator()
        {
            return this.Fluent();
        }

        public class Validator : FluentValidator<SayHelloRequest>
        {
            protected override void ExpressValidationRules()
            {
                RuleFor(x => x.Name).NotEmpty();
            }
        }
    }
}