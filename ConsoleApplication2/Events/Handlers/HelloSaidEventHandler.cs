﻿using System;
using ConsoleApplication2.Concept.Events;

namespace ConsoleApplication2.Events.Handlers
{
    public class HelloSaidEventHandler : IHandle<HelloSaid>
    {
        public void Handle(HelloSaid args)
        {
            Console.WriteLine("handle hello said event");
        }
    }
}
