﻿using ConsoleApplication2.Concept.Events;

namespace ConsoleApplication2.Events
{
    public class HelloSaid : IApplicationEvent
    {
        public HelloSaid(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}
