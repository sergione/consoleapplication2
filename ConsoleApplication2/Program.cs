﻿using System;
using System.Collections.Generic;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var request = new SayHelloRequest {Name = "Sergiu"};

            var result = Execute(request);

            Console.WriteLine("Command Success: " + result.Success);
        }

        public static ResultModel<TResponse> Execute<TResponse>(IRequest<TResponse> request) where TResponse : class, IResponse
        {
            var result = new ResultModel<TResponse>();

            var validationContext = ServiceLocator.Instance.Get<IValidationContext>();

            request.CreateValidator().Validate(validationContext);

            foreach (var validationError in validationContext.ValidationErrors)
            {
                result.Success = false;

                result.Messages.Add(new ResultMessage
                {
                    Context = validationError.Key,
                    Message = validationError.Message
                });
            }

            if (!result.Success)
            {
                return result;
            }

            var requestExecutor = ServiceLocator.Instance.Get<IRequestExecutor>();

            var response = requestExecutor.Execute(request);

            result.Response = response;

            return result;
        }
    }

    public class ResultModel<TResponse>
    {
        private bool? _success;

        public ResultModel()
        {
            Messages = new List<ResultMessage>();
            Info = new List<KeyValuePair<string, string>>();
        }

        public bool Success
        {
            get
            {
                if (_success.HasValue)
                {
                    return _success.Value;
                }

                return true;
            }

            set { _success = value; }
        }

        public TResponse Response { get; set; }

        public ICollection<ResultMessage> Messages { get; private set; }
        
        public ICollection<KeyValuePair<string, string>> Info { get; set; }
    }

    public class ResultMessage
    {
        public string Context { get; set; }

        public string Message { get; set; }
    }
}
