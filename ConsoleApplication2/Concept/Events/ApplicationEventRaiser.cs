﻿using Ninject;
using Ninject.Extensions.Conventions;

namespace ConsoleApplication2.Concept.Events
{
    public interface IApplicationEventRaiser
    {
        void Raise<T>(T args) where T : IApplicationEvent;
    }

    public class ApplicationEventRaiser : IApplicationEventRaiser
    {
        public virtual void Raise<T>(T args) where T : IApplicationEvent
        {
            foreach (var handler in ServiceLocator.Instance.GetAll<IHandle<T>>())
            {
                handler.Handle(args);
            }
        }
    }
}
