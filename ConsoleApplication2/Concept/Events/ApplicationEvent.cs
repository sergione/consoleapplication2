﻿using Ninject;
using Ninject.Extensions.Conventions;

namespace ConsoleApplication2.Concept.Events
{
    public interface IApplicationEvent
    {    
    }

    public interface IHandle<T> where T : IApplicationEvent
    {
        void Handle(T args);
    }

    public static class ApplicationEvent
    {
        public static void Raise<T>(T args) where T : IApplicationEvent
        {
            var raiser = ServiceLocator.Instance.Get<IApplicationEventRaiser>();
            raiser.Raise(args);
        }
    }
}
