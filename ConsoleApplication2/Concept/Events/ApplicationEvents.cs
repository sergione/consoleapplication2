﻿using Ninject;
using Ninject.Extensions.Conventions;

namespace ConsoleApplication2.Concept.Events
{
    public static class ApplicationEvents
    {
        public static void Raise<T>(T args) where T : IApplicationEvent
        {
            var raiser = ServiceLocator.Instance.Get<IApplicationEventRaiser>();
            raiser.Raise(args);
        }
    }
}
