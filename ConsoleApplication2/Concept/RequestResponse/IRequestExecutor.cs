namespace ConsoleApplication2
{
    public interface IRequestExecutor
    {
        TResponse Execute<TResponse>(IRequest<TResponse> request) where TResponse : class, IResponse;
    }
}