﻿using System.Collections.Generic;
using ConsoleApplication2.Concept.Validation;

namespace ConsoleApplication2
{
    public interface IRequestHandler<T> : IRequestHandler
    {
    }

    public interface IRequestHandler : IRequireValidation
    {
        void SetRequest(object request);
        void Execute();

        object GetResponse();

        IEnumerable<ValidationSource> CreateValidatorSources();
    }
}