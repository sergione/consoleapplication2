﻿using System.Collections.Generic;
using ConsoleApplication2.Concept.Validation;

namespace ConsoleApplication2
{
    public abstract class RequestHandler<TRequest, TResponse> : IRequestHandler<TRequest> 
        where TRequest : IRequest<TResponse>
        where TResponse : class, IResponse, new()
    {
        public RequestHandler()
        {
            Response = new TResponse();
        } 

        public TRequest Request { get; set; }

        public TResponse Response { get; set; }

        public void SetRequest(object request)
        {
            Request = (TRequest) request;
        }

        public abstract void Execute();

        public object GetResponse()
        {
            return Response;
        }

        protected abstract IValidate CreatePermissionValidator();

        IValidate IRequireValidation.CreateValidator()
        {
            return this.NullValidator();
        }

        IEnumerable<ValidationSource> IRequestHandler.CreateValidatorSources()
        {
            yield return CreatePermissionValidator().AsSource("Permissions");
        } 
    }
}