using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ConsoleApplication2.Concept.Aspect;
using Ninject;
using Ninject.Extensions.Conventions;

namespace ConsoleApplication2
{
    public class RequestExecutor : IRequestExecutor
    {
        public TResponse Execute<TResponse>(IRequest<TResponse> request) where TResponse : class, IResponse
        {
            var handlerType = typeof (IRequestHandler<>).MakeGenericType(request.GetType());

            var handler = (IRequestHandler) ServiceLocator.Instance.Get(handlerType);

            handler.SetRequest(request);

            var validationContext = new ValidationContext();

            foreach (var validationSource in handler.CreateValidatorSources())
            {
                validationSource.Validator.Validate(validationContext);
            }

            if (validationContext.IsValid)
            {
                var aspectStack = new Stack<IRequestHandlerAspect>();
                var handleRequest = true;

                foreach (var aspectType in new AspectOrderDictionary().OrderBy(x => x.Value).ToArray())
                {
                    var aspect = (IRequestHandlerAspect)Activator.CreateInstance(aspectType.Key);

                    if (!aspect.OnBegin(handler))
                    {
                        handleRequest = false;
                        break;
                    }

                    aspectStack.Push(aspect);
                }

                if (handleRequest)
                {
                    handler.Execute();
                }

                while (aspectStack.Any())
                {
                    var aspect = aspectStack.Pop();

                    aspect.OnEnd(handler);
                }
            }

            var response = handler.GetResponse();
            return response == null ? null : (TResponse) response;
        }
    }
}