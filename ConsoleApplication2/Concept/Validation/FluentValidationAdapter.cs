﻿using Ninject;
using Ninject.Extensions.Conventions;

namespace ConsoleApplication2
{
    public class FluentValidationAdapter<T> : IValidate
    {
        private readonly T _input;

        public FluentValidationAdapter(T instance)
        {
            _input = instance;
        }

        public void Validate(IValidationContext validationContext)
        {
            var validatorType = typeof (IFluentValidator<>).MakeGenericType(typeof (T));

            var validator = (IFluentValidator) ServiceLocator.Instance.Get(validatorType);

            var results = validator.Validate(_input);

            if (!results.IsValid)
            {
                foreach (var error in results.Errors)
                {
                    validationContext.AddValidationError(error.PropertyName, error.ErrorMessage);
                }
            }
        }
    }
}