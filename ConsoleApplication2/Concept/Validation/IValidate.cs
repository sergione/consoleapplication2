namespace ConsoleApplication2
{
    public interface IValidate
    {
        void Validate(IValidationContext validationContext);
    }
}