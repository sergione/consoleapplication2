using ConsoleApplication2.Concept.Validation;

namespace ConsoleApplication2
{
    public static class RequireValidationExtensions
    {
        public static IValidate NullValidator(this IRequireValidation instance)
        {
            return NullObjectValidator.Instance;
        }

        public static IValidate Fluent<T>(this T instance)
            where T: IRequireValidation
        {
            var validator = new FluentValidationAdapter<T>(instance);
            return validator;
        }

        public static IValidate HasPermissions(this IRequireValidation instance, params string[] permissions)
        {
            return new AllPermissionsValidator(permissions);
        }
    }

    public static class ValidatorExtensions
    {
        public static ValidationSource AsSource(this IValidate validator, string source)
        {
            return new ValidationSource(validator, source);
        }
    }
}