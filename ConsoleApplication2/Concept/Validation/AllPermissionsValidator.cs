using System;

namespace ConsoleApplication2
{
    public class AllPermissionsValidator : IValidate
    {
        private readonly string[] _permissions;

        public AllPermissionsValidator(string[] permissions)
        {
            _permissions = permissions;
        }

        public void Validate(IValidationContext validationContext)
        {
            // do the validation
            Console.WriteLine("do the permissions validation");
        }
    }
}