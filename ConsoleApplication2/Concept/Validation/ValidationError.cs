﻿namespace ConsoleApplication2
{
    public class ValidationError
    {
        public string Key { get; set; }

        public string Message { get; set; }
    }
}