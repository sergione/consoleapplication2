﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2.Concept.Validation
{
    public class ValidationSource
    {
        public ValidationSource(IValidate validator, string source)
        {
            Source = source;
            Validator = validator;
        }

        public string Source { get; private set; }

        public IValidate Validator { get; private set; }
    }
}
