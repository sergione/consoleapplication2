﻿namespace ConsoleApplication2
{
    public interface IRequireValidation
    {
        IValidate CreateValidator();
    }
}