﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2.Concept.Aspect
{
    public class HelloAspect : RequestHandlerAspect
    {
        public override bool OnBegin(IRequestHandler handler)
        {
            Console.WriteLine("begin aspect");
            return true;
        }

        public override bool OnEnd(IRequestHandler handler)
        {
            Console.WriteLine("end aspect");
            return true;
        }
    }
}
