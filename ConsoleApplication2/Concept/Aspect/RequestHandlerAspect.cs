﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2.Concept.Aspect
{
    public interface IRequestHandlerAspect
    {
        int Order { get; }

        bool OnBegin(IRequestHandler handler);

        bool OnEnd(IRequestHandler handler);
    }

    public abstract class RequestHandlerAspect : IRequestHandlerAspect
    {
        private static readonly AspectOrderDictionary OrderMapping = new AspectOrderDictionary();

        private readonly string _aspectName;

        public RequestHandlerAspect()
        {
            _aspectName = GetType().Name;
        }

        public int Order
        {
            get { return OrderMapping.GetAspectOrder(GetType()); }
        }

        public abstract bool OnBegin(IRequestHandler handler);

        public abstract bool OnEnd(IRequestHandler handler);
    }

    public class AspectOrderDictionary : Dictionary<Type, int>
    {
        public AspectOrderDictionary()
        {
            Add(typeof(HelloAspect), 1);
        }

        public int GetAspectOrder(Type type)
        {
            return this[type];
        }
    }
}
