﻿namespace ConsoleApplication2
{
    public interface IRequest<out TResponse> : IRequest
        where TResponse : IResponse
    {
        
    }

    public interface IRequest : IRequireValidation
    {
    }
}