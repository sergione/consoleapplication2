﻿namespace ConsoleApplication2
{
    public abstract class RequestHandler<TRequest, TResponse> : IRequestHandler<TRequest> 
        where TRequest : IRequest<TResponse>
        where TResponse : class, IResponse, new()
    {
        public RequestHandler()
        {
            Response = new TResponse();
        } 

        public TRequest Request { get; set; }

        public TResponse Response { get; set; }

        public void SetRequest(object Request)
        {
            Request = (TRequest) Request;
        }

        public abstract void Handle();

        public object GetResponse()
        {
            return Response;
        }
    }
}