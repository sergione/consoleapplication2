﻿namespace ConsoleApplication2
{
    public interface IRequestHandler<T> : IRequestHandler
    {
    }

    public interface IRequestHandler
    {
        void SetRequest(object Request);
        void Handle();

        object GetResponse();
    }
}