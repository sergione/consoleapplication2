﻿using FluentValidation.Results;

namespace ConsoleApplication2
{
    public interface IFluentValidator<in T> : IFluentValidator
    {
        
    }

    public interface IFluentValidator
    {
        ValidationResult Validate(object instance);
    }
}