﻿using FluentValidation;
using FluentValidation.Results;

namespace ConsoleApplication2
{
    public abstract class FluentValidator<T> : AbstractValidator<T>, IFluentValidator<T>
    {
        protected FluentValidator()
        {
            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            ExpressValidationRules();
        }

        ValidationResult IFluentValidator.Validate(object instance)
        {
            return Validate((T) instance);
        }

        protected abstract void ExpressValidationRules();
    }
}