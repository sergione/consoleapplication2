namespace ConsoleApplication2
{
    public class NullObjectValidator : IValidate
    {
        public static readonly IValidate Instance = new NullObjectValidator();

        private NullObjectValidator()
        {
            
        }

        public void Validate(IValidationContext validationContext)
        {
            // Do nothing.
        }
    }
}