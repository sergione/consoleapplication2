using System.Collections.Generic;
using System.Linq;

namespace ConsoleApplication2
{
    public class ValidationContext : IValidationContext
    {
        public ValidationContext()
        {
            ValidationErrors = new List<ValidationError>();
        }

        public List<ValidationError> ValidationErrors { get; private set; }

        public bool IsValid => !ValidationErrors.Any();

        public void AddValidationError(string key, string message)
        {
            var error = new ValidationError()
            {
                Key = key,
                Message = message
            };

            ValidationErrors.Add(error);
        }
    }
}