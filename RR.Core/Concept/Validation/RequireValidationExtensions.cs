namespace ConsoleApplication2
{
    public static class RequireValidationExtensions
    {
        public static IValidate NullValidator(this IRequireValidation instance)
        {
            return NullObjectValidator.Instance;
        }

        public static IValidate Fluent<T>(this T instance)
            where T: IRequireValidation
        {
            var validator = new FluentValidationAdapter<T>(instance);
            return validator;
        }
    }
}