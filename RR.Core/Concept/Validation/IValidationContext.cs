using System.Collections.Generic;

namespace ConsoleApplication2
{
    public interface IValidationContext
    {
        bool IsValid { get; }

        List<ValidationError> ValidationErrors { get; }

        void AddValidationError(string key, string message);
    }
}